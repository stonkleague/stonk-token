// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts@4.3.3/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts@4.3.3/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts@4.3.3/security/Pausable.sol";
import "@openzeppelin/contracts@4.3.3/access/Ownable.sol";

contract ROCI is ERC20, ERC20Burnable, Pausable, Ownable {
    uint256 private TOTAL_SUPPLY = 1100 * (10 ** 6); // 1.1 billion
    uint256 private dec;
    constructor() ERC20("ROCI", "ROCI") {
        dec = 10 ** decimals();
        _mint(payable(msg.sender), TOTAL_SUPPLY * dec);
    }

    function pause() public onlyOwner {
        _pause();
    }

    function unpause() public onlyOwner {
        _unpause();
    }

    function _beforeTokenTransfer(address from, address to, uint256 amount)
    internal
    whenNotPaused
    override
    {
        super._beforeTokenTransfer(from, to, amount);
    }
}
